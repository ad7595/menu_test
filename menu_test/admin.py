from django.contrib import admin
from .models import Menu, MenuCategory


class MenuCategoryAdmin(admin.ModelAdmin):
    fields = ['name',]
    list_display = ['name',]


class MenuAdmin(admin.ModelAdmin):
    fields = ['name', 'category',]
    list_display = ['name', 'category',]


admin.site.register(Menu, MenuAdmin)
admin.site.register(MenuCategory, MenuCategoryAdmin)
