from django.db import models


class MenuCategory(models.Model):
    """Категории меню."""
    name = models.CharField(
        'Название категории',
        max_length=100,
        unique=True
    )

    class Meta:
        verbose_name = 'Категория меню'
        verbose_name_plural = 'Категории меню'

    def __str__(self):
        return str(self.name)


class Menu(models.Model):
    """Меню."""
    name = models.CharField(
        'Название',
        max_length=100,
        unique=True
    )
    category = models.ForeignKey(
        MenuCategory,
        'Категория меню',
    )

    class Meta:
        verbose_name = 'Меню'
        verbose_name_plural = 'Меню'

    def __str__(self):
        return str(self.name)
